close all; clear all; 

DATA = load('gps_ex1_morningdrive2012.txt');
Longitude = DATA(:,4); % read all rows in column 4
Latitude  = DATA(:,3); % read all rows in column 3

LongDeg = floor(Longitude/100) + (Longitude - floor(Longitude/100)*100)/60;
LatDeg = floor(Latitude/100) + (Latitude - floor(Latitude/100)*100)/60;

F_lon = 62393;
F_lat = 111342;

X = F_lon * LongDeg;
Y = F_lat * LatDeg;
[im, map] = imread('halmstad_drive_area.gif');
imshow(im, map)
print(1, '-dpng', 'map');

start = 240;
duration = 70;
figure(2)
plot(X(1:start-1), Y(1:start-1), 'k',  ...
    X(start:start+duration), Y(start:start+duration), 'b', ...
    X(start+duration:end), Y(start+duration:end), 'k')

print(2, '-dpng', 'path');
velocity = sqrt((diff(X).^2 + diff(Y).^2)./diff(DATA(:, 2)));
velocity = velocity * 3.6;
disp('max velocity is')
max(velocity)
figure(3)
subplot(2,1,1); 
plot([1:start-1], velocity(1:start-1), 'k', ...
     [start:start+duration], velocity(start:start+duration), 'b', ...
     [start+duration:size(velocity, 1)], velocity(start+duration:end), 'k')
title('Velocity')

diff_LongDeg = diff(LongDeg);
diff_LatDeg = diff(LatDeg);
for i=1:size(diff_LongDeg, 1)
    if (diff_LongDeg(i) == 0)
        heading(i) = pi;
    else
        if (diff_LongDeg(i) > 0)
            heading(i) = pi/2 - ...
				atan((F_lat*diff_LatDeg(i))/(F_lon*diff_LongDeg(i)));
        else
            heading(i) = 3*pi/2 - ...
				atan((F_lat*diff_LatDeg(i))/(F_lon*diff_LongDeg(i)));
        end
    end
end
heading = heading*180/pi;
subplot(2,1,2);
plot([1:start-1], heading(1:start-1), 'k', ...
     [start:start+duration], heading(start:start+duration), 'b', ...
     [start+duration:size(heading, 2)], heading(start+duration:end), 'k')
title('Heading')
print(3, '-dpng', 'speed_heading');
disp('variance of heading going along particular street')
var(heading(start:start+duration))
