% Stationary GPS receiver 
% (c) Bj�rn �strand, 2012
close all; clear all; 

DATA = load('gps_ex1_window2012.txt');
Longitude = DATA(:,4); % read all rows in column 4
Latitude  = DATA(:,3); % read all rows in column 3
figure(1),clf, plot(Longitude,Latitude, 'x');
title('Position in NMEA-0183 format');
xlabel('Longitude');
ylabel('Latitude');
print(1, '-dpng', 'nmea');
% 3.1 Write a function that transform your longitude and latitude angles 
% from NMEA-0183 into meters
% 1. longitude and latitude angles from NMEA-0183 into degrees  

LongDeg = floor(Longitude/100) + (Longitude - floor(Longitude/100)*100)/60;
LatDeg = floor(Latitude/100) + (Latitude - floor(Latitude/100)*100)/60;

figure(2), clf, plot(LongDeg,LatDeg, 'o');
title('Position in degrees');
xlabel('Longitude');
ylabel('Latitude');
print(2, '-dpng', 'degree');

% 2. longitude and latitude angles from NMEA-0183 into degrees
F_lon = 62393;
F_lat = 111342;

X = F_lon * LongDeg;
Y = F_lat * LatDeg;

figure(3), clf, plot(X,Y, 'o');
title('Position in meters');
xlabel('Longitude');
ylabel('Latitude');
print(3, '-dpng', 'meter');

% 3.2 Estimate the mean and variance of the position (in x and y)
% Matlab fuctions mean() and var()
% -> Your code here
xerror = X - mean(X);
figure(4), clf, histfit(xerror, 30)
title('X Error');
print(4, '-dpng', 'fit_x');

yerror = Y - mean(Y);
figure(5), clf, histfit(yerror, 30)
title('Y Error');
print(5, '-dpng', 'fit_y');

max(xerror)
max(yerror)

covariance = cov(xerror, yerror);
figure(6), 
plot(xerror, yerror, 'x'), hold on, 
plot_uncertainty([0 0]', covariance, 1, 2)
title('Error')
legend('Error', 'Covariance matrix')
print(6, '-dpng', 'covarianceMatrix');
% 3.3 Plot, with respect to time, the errors and the auto-correlation 
% in x and y separately.
cx = xcorr(xerror, 'coeff');
cy = xcorr(yerror, 'coeff');
R = randn(1, length(X));
cn = xcorr(R-mean(R), 'coeff');
figure(7),clf
subplot(4,1,1), plot(xerror)
subplot(4,1,2), plot(yerror)
subplot(4,1,3), plot(cx)
hold on, plot(cn, 'r')
subplot(4,1,4), plot(cy)
hold on, plot(cn, 'r')
print(7, '-dpng', 'comparison');