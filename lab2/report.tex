\documentclass[a4paper, 11pt]{article}
\usepackage[left=3cm]{geometry}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{upgreek}
\usepackage{verbatim}

\include{code}

\begin{document}
\title{Lab2 report}
\author{Mingkun Yang}
\date{}
\maketitle
\begin{abstract}
    This article uses two robot models, one with a differential drive (the Khepera mini robot) and the other with a driving wheel in the front of the
    vehicle (the snowwhite robot), to demonstrate how dead reckoning based on odometry is used to locate robot itself. Furthermore, how uncertainties
    of last movement are transformed to state variables is shown.
\end{abstract}

\section{Introduction}
A indispensable task for any mobile system is to know how state variables ($x$, $y$, $\theta$ in this case) are affected by the last movement. Dead
reckoning combined with odometry is usually used to solve this problem. At last, how uncertainties are updated because of the new uncertainties
introduced by the last movement is addressed.
\section{Khepera mini robot}
\subsection{System model and covariance matrix}
Changes expressed in the robot co-ordinate system caused by the latest movement\cite{wang}:
\begin{eqnarray}
    \Delta d = \frac{\Delta d_r + \Delta d_l}{2} \\
    \Delta \theta = \frac{\Delta d_r - \Delta d_l}{WheelBase}
\end{eqnarray}
According to the above relation, the covariance matrix of $\Delta d$ and $\Delta \theta$ is
$$
cov<\Delta d, \Delta \theta> =
\begin{pmatrix}
    \frac{\sigma_l^2+\sigma_r^2}{4} & 0 \\
    0 & \frac{\sigma_l^2+\sigma_r^2}{L^2}
\end{pmatrix}$$
, where $L$ is WheelBase and we assume that $\sigma_r = \sigma_l = 0.5/12$.

\subsection{Circular trajectory}
Iterate through all the data and in each iteration, we update the state variables and covariance matrix. In the end, we plot the covariance matrix.
Figure~\ref{fig:without} presents the trajectory taken by this robot.

\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.4]{images/circle_without.png}
    \caption{Model without the compensation term}
    \label{fig:without}
\end{figure}

\subsection{Compensation term}
Since $\Delta \theta$ is very small, the compensation term is more or less equal to $1$. Therefore, the estimated state variables and covariance
matrices are more or less the same. If the frequency decreases, the above assumption is not valid any more, then the state variables and covariance
matrices will differ.
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.4]{images/circle_with.png}
    \caption{Model with the compensation term}
    \label{fig:with}
\end{figure}

\subsection{Evolving of covariance matrix}
According to the Figure~\ref{fig:with}, the eigenvalues of the covariance matrix is getting bigger. As time goes, the uncertainty increases, so it's
realistic.

\subsection{Another model}
Not surprisingly, the circle becomes smaller, as showed in Figure~\ref{fig:compare}. New model is constructed using the new variables.
$$ X_k = X_{k-1} +
\begin{bmatrix}
    \frac{\Delta R + \Delta L}{2} cos(\theta_{k-1} + \frac{\Delta R - \Delta L}{2b}) \\
    \frac{\Delta R + \Delta L}{2} sin(\theta_{k-1} + \frac{\Delta R - \Delta L}{2b}) \\
    \frac{\Delta R - \Delta L}{b}
\end{bmatrix}
$$
According to law of error propagation, new uncertainties can be incorporated into the total uncertainty.

$$ \Upsigma_{X_k} = J_{X_{k-1}} \Upsigma_{X_{k-1}} J_{X_{k-1}}^T
+ J_{\Delta R \Delta L} \Upsigma_{\Delta R \Delta L} J_{\Delta R \Delta L}^T
+ J_b \Upsigma_b J_b^T$$
, where $ \Upsigma_{\Delta R \Delta L} =
\begin{bmatrix}
    k_r \Delta R & 0\\
    0  & k_l \Delta L
\end{bmatrix}, and k_r = k_l = 0.1, \Upsigma_b = (53-45)/53
$
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.4]{images/compare.png}
    \caption{Comparison of two models with difference d and WheelBase}
    \label{fig:compare}
\end{figure}

\subsection{Non-circular trajectory}
All the parameters are preserved; only the path taken is different.
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.4]{images/noncircle_without.png}
    \caption{Model without the compensation term}
\end{figure}
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.4]{images/noncircle_with.png}
    \caption{Model with the compensation term}
\end{figure}
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.4]{images/noncircle_compare.png}
    \caption{Comparison of two models with difference d and WheelBase}
\end{figure}

\section{Snowwhite}
\subsection{System model}
Firstly, construct the model and its corresponding covariance matrix:
\begin{eqnarray*}
    X_k &=& X_{k-1} +
    \begin{bmatrix}
        \Delta d cos(\theta_{k-1} + \frac{\Delta \theta}{2}) \\
        \Delta d sin(\theta_{k-1} + \frac{\Delta \theta}{2}) \\
        \Delta \theta
    \end{bmatrix} \\
    &=&
    \begin{bmatrix}
        x_{k-1} + vcos(\alpha) T cos(\theta_{k-1} + \frac{v sin(\alpha) T}{2L}) \\
        y_{k-1} + vcos(\alpha) T sin(\theta_{k-1} + \frac{v sin(\alpha) T}{2L}) \\
        \theta_{k-1} + \frac{v sin(\alpha) T}{L}
    \end{bmatrix}
\end{eqnarray*}

$$ \Upsigma_{X_k} = J_{X_{k-1}} \Upsigma_{X_{k-1}} J_{X_{k-1}}^T + J_{v \alpha T} \Upsigma_{v \alpha T} J_{v \alpha T}^T
$$

Jacobian matrix is obtained using Matlab built-in function as shown in \ref{l:jacobian}:

\begin{lstlisting}[label=l:jacobian,caption=Jacobian]
syms x y theta
syms velocity alpha T
f = [
    x + velocity*cos(alpha)*T*cos(theta + velocity*sin(alpha)*T/(2*L))
    y + velocity*cos(alpha)*T*sin(theta + velocity*sin(alpha)*T/(2*L))
    theta + velocity*sin(alpha)*T/L
];
f_xytheta = jacobian(f, [x, y, theta]);
f_velocityalphaT = jacobian(f, [velocity, alpha, T]);
\end{lstlisting}

\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.4]{images/path.png}
    \caption{Path taken by this robot}
\end{figure}

\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.4]{images/error.png}
    \caption{Errors in state variables}
    \label{f:e_state}
\end{figure}
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.4]{images/errorVariance.png}
    \caption{Errors in state variables estimates}
    \label{f:e_estimate}
\end{figure}
\subsection{Errors}
Figure~\ref{f:e_state} and Figure~\ref{f:e_estimate} show the error in state variables and their estimations, respectively.

\subsection{Distance and time}
The total distance is 36157 mm and time is 202.5 seconds.

\section{Conclusion}
Dead reckoning provides one good way to predict robot's state within a short range, but the error grows without the bound over the long run.
\begin{thebibliography}{9}
    \bibitem{wang}
        \emph{LOCATION ESTIMATION AND UNCERTAINTY ANALYSIS FOR MOBILE ROBOTS}
\end{thebibliography}
\end{document}
