clear all;
close all;

L = 680;
T_value = 0.05;

syms x y theta
syms velocity alpha T
f = [
     x + velocity*cos(alpha)*T*cos(theta + velocity*sin(alpha)*T/(2*L))
     y + velocity*cos(alpha)*T*sin(theta + velocity*sin(alpha)*T/(2*L))
     theta + velocity*sin(alpha)*T/L
     ];
f_xytheta = jacobian(f, [x, y, theta]);
f_velocityalphaT = jacobian(f, [velocity, alpha, T]);


DATA = load('snowhite.txt');
N = size(DATA, 1);
% Initial state and uncertainty
X = zeros(size(DATA, 1), 1);
Y = zeros(size(DATA, 1), 1);
Theta = zeros(size(DATA, 1), 1);
X(1) = DATA(1, 3);
Y(1) = DATA(1, 4);
Theta(1) = DATA(1, 5);
P(1,1:9) = [1 0 0 0 1 0 0 0 (1*pi/180)^2];

Cvat = [1 0 0; 0 (1*pi/180)^2 0; 0 0 0.001^2];

for i = 2:N
    dD = DATA(i-1, 1)*cos(DATA(i-1, 2)) * T_value;
    
    dTheta = DATA(i-1, 1)*sin(DATA(i-1, 2)) * T_value / L;
    dX = dD*cos(Theta(i-1) + dTheta/2);
    dY = dD*sin(Theta(i-1) + dTheta/2);
    
    X(i) = X(i-1) + dX;
    Y(i) = Y(i-1) + dY;
    Theta(i) = mod(Theta(i-1) + dTheta, 2*pi);
    
    Cxytheta_old = [P(i-1, 1:3); P(i-1, 4:6); P(i-1, 7:9)];
    
    Axytheta = subs(f_xytheta, ...
        {theta, velocity, alpha, T}, ...
		{Theta(i-1), DATA(i-1, 1), DATA(i-1, 2), T_value});
    Avat = subs(f_velocityalphaT,  ...
        {theta, velocity, alpha, T}, ...
		{Theta(i-1), DATA(i-1, 1), DATA(i-1, 2), T_value});
    
    Cxytheta_new = Axytheta*Cxytheta_old*Axytheta' + Avat*Cvat*Avat';
   
    P(i, 1:9) = [Cxytheta_new(1, :) ...
		Cxytheta_new(2, :) Cxytheta_new(3, :)];
end

figure(1)
for kk = 1:N,
    C = [P(kk,1:3);P(kk,4:6);P(kk,7:9)];
    plot_uncertainty([X(kk) Y(kk) Theta(kk)]', C, 1, 2);
end;
print(1, '-dpng', 'path');
figure(2)
title('Error in state variables')
subplot(3,1,1);plot(abs(X-DATA(:, 3)))
subplot(3,1,2);plot(abs(Y-DATA(:, 4)))
subplot(3,1,3);plot(mod(abs(Theta-DATA(:, 5)), 2*pi))
print(2, '-dpng', 'error');
figure(3)
title('% Error in variances');
subplot(3,1,1);plot(abs(abs(X-DATA(:, 3) - sqrt(P(:,1)))))
subplot(3,1,2);plot(abs(abs(Y-DATA(:, 4) - sqrt(P(:,5)))))
subplot(3,1,3);plot(abs(mod(abs(Theta-DATA(:, 5) - sqrt(P(:,9))), 2*pi)))
print(3, '-dpng', 'errorVariance');
disp('The distance[mm] of this path: ')
sum(DATA(:,1) * T_value)
disp('The total time[sec]: ')
N * T_value
