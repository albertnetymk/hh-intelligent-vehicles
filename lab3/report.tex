\documentclass[a4paper, 11pt]{article}
\usepackage[a4paper]{geometry}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{array}
\usepackage{amsmath}
\usepackage{subfig}


\begin{document}
\title{Lab3\&4 report}
\author{Mingkun Yang}
\maketitle

\begin{abstract}
    With only dead reckoning, the error grows without bounds. In this project, we are going to build the relation between the observation and the 
    prior map to limit the errors. Since we have two sources of information concerning location, we need some approaches to combine them, which is 
    what sensor fusion is about. In this case, we will use Kalman filter for this purpose.
\end{abstract}
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.6]{images/scan.png}
    \caption{Laser scanning sample}
    \label{fig:laser}
\end{figure}
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.6]{images/error.png}
    \caption{Error and variance of position}
    \label{fig:error}
\end{figure}
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.6]{images/path.png}
    \caption{The path taken by the robot}
    \label{fig:path}
\end{figure}
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.4]{images/cox_line.png}
    \caption{Result caused by error in localization}
    \label{fig:cox_line}
\end{figure}
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.6]{images/path_cox.png}
    \caption{The path taken by the robot with Cox fix}
    \label{fig:path_cox}
\end{figure}
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.6]{images/error_cox.png}
    \caption{Error and variance of position with Cox fix}
    \label{fig:error_cox}
\end{figure}
\section{Analysis}
The robot is equipped with one laser sensor, which can provide the distance from our robot to the obstacle and the corresponding angle.
Figure~\ref{fig:laser} illustrates how laser works by presents some laser scanning data.

Figure~\ref{fig:error} shows that error and uncertainty of dead reckoning position grows gradually. Since the path of the robot is round, the error
reflects some periodicity. Every two peaks represent one revolution, so the robot is running for three revolutions, which coincides with the
information from Figure~\ref{fig:path}

\section{Cox algorithm}
Cox algorithm is used to match the points (data) obtained from laser with the map stored previously, and it's detailed described in \cite{cox_paper}.
The essence of this algorithm is repeated in here so that readers who don't want to read the full paper would still get the gist.

The error in localization will probably create results shown in Figure~\ref{fig:cox_line}. The task is to find one congruence so that the points will
be placed onto the corresponding lines, and such congruence is described by $(\mathbf{t}, \theta)$, one rotation and one translation. The rotation
center is the center of all points, denoted as $\mathbf{\bar{v}}$. Then the rotation could be written as:

\begin{equation*}
    \left[
        \begin{array}{cc}
            cos(\theta) & -sin(\theta) \\
            sin(\theta) & cos(\theta)
        \end{array}
    \right]
    (\mathbf{z} - \mathbf{\bar{v}}) + \mathbf{\bar{v}}
\end{equation*}

Apply first-order approximation, or pseudorotation, the above equation becomes:
\begin{equation*}
    \left[
        \begin{array}{cc}
            0 & -\theta \\
            \theta & 0
        \end{array}
    \right]
    (\mathbf{z} - \mathbf{\bar{v}}) + \mathbf{\bar{v}}
    = \theta
    \left[
        \begin{array}{cc}
            0 & -1 \\
            1 & 0
        \end{array}
    \right]
    (\mathbf{z} - \mathbf{\bar{v}}) + \mathbf{\bar{v}}
\end{equation*}

Combined with translation, the whole congruence becomes:
\begin{equation*}
    \mathbf{t} + \theta
    \left[
        \begin{array}{cc}
            0 & -1 \\
            1 & 0
        \end{array}
    \right]
    (v_i - \mathbf{\bar{v}}) + v_i
\end{equation*}

With proper definition of $\mathbf{b}$, it could be rewritten in the following form, which is much convenient to do numerical analysis in Matlab.

\begin{equation*}
    \mathbf{X} \mathbf{b} \simeq \mathbf{y}
\end{equation*}

Figure~\ref{fig:path_cox} shows the path of the robot, and Figure~\ref{fig:error_cox} shows localization error and uncertainty with Cox fix.
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.6]{images/path_cox_sim1.png}
    \caption{The path taken by the robot with lower deviation}
    \label{fig:path_cox_sim1}
\end{figure}
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.6]{images/error_cox_sim1.png}
    \caption{Error and variance of position with lower deviation}
    \label{fig:error_cox_sim1}
\end{figure}
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.6]{images/path_cox_sim2.png}
    \caption{The path taken by the robot with higher deviation}
    \label{fig:path_cox_sim2}
\end{figure}
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.6]{images/error_cox_sim2.png}
    \caption{Error and variance of position with higher deviation}
    \label{fig:error_cox_sim2}
\end{figure}
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.6]{images/path_cox_real.png}
    \caption{The path taken by the robot using sensor fusion with Cox fix}
    \label{fig:path_cox_real}
\end{figure}
\begin{figure}[hp]
    \centering
    \includegraphics[scale=0.6]{images/error_cox_real.png}
    \caption{Error and variance of position using sensor fusion with Cox fix}
    \label{fig:error_cox_real}
\end{figure}
\section{Kalman filter}
Before using Kalman filter to integrate the result from Cox fix, we will prepare ourselves by firstly trying some artificial data. The general idea of
Kalman filter is to combine two or more independent sensor sources to obtain one result with less certainty than any single of sensor source. The
actual formula is given in lab manual.

\subsection{Small deviation}
Figure~\ref{fig:path_cox_sim1} and Figure~\ref{fig:error_cox_sim1} show the result of sensor fusion between our primitive data and articulated
Gaussian noise with standard deviation of position $10mm$ and orientation $1^{\circ}$.

\subsection{Large deviation}
Similarly, Figure~\ref{fig:path_cox_sim2} and Figure~\ref{fig:error_cox_sim2} show the result of sensor fusion with higher deviation, $100mm$ for
position and $3^{\circ}$ for orientation precisely. Comparing with previous case, small deviation, it's obvious that the position is estimated poorly.

\subsection{Use result from cox algorithm}
After that, we will use the result from Cox for sensor fusion in Figure~\ref{fig:path_cox_real} and Figure~\ref{fig:error_cox_real}. It's obvious to
see that the errors are much reduced from both plots.

\section{Conclusion}
Dead reckoning is one good method for self localization, but the accumulated error has be calibrated using external environment measurement. Cox
algorithm is one simple and effect way for this problem according to the result obtained in this example.

\begin{thebibliography}{9}
    \bibitem{cox_paper}
        \emph{Blanche An Experiment in Guidance and Navigation of an Autonomous Robot Vehicle}
    \bibitem{gps_table}
        \emph{GPS Lookup Table}
        http://bse.unl.edu/adamchuk/web\_ssm/web\_GPS\_tb.html
\end{thebibliography}
\end{document}
