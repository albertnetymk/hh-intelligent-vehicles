% Fits (by translation and rotation) data points to a set of 
% line segments, i.e. applying the Cox's algorithm
% function [ddx,ddy,dda,C] = Cox_LineFit(ANG, DIS, POSE, LINEMODEL)
% (ANG, DIS) ([1xN], [1xN]) contains the pair of values obtained from laser sensor.
function [ddx,ddy,dda,C] = Cox_LineFit2012(ANG, DIS, POSE, LINEMODEL, SensorPose)
    % Init variables
    ddx = 0; ddy = 0; dda = 0;
	% State variables.
    X = POSE(1,1); Y = POSE(2,1); Theta = POSE(3,1);
    x_offset = SensorPose(1);
    y_offset = SensorPose(2);
    alpha = SensorPose(3);
    max_iterations = 15;
    no_update = 0;
	% To test whether this image point is an outlier. Unit is [mm]
    DISTANCE_THRESHOLD = 100;
  
    REF = [1920 9470;   % 01
       10012 8179;  % 02
       9770 7590;   % 03
       11405 7228;  % 04
       11275 6451;  % 05
       11628 6384.5;% 06
       11438 4948;  % 07
       8140 8274;   % 08
       8392 8486;   % 09
       3280 2750;   % 10
       7250 2085;   % 11
       9990 1620;   % 12
       7485 3225;   % 13
       9505 3893;   % 14
       9602 4278;   % 15
       10412 4150;  % 16
       4090 7920;   % 17
       8010 5290;   % 18
       8255 6099;   % 19
       7733 6151;   % 20
       7490 6136;   % 21
       7061 5420;   % 22
       7634 5342];  % 23

% LINE SEGMENT MODEL (Indeces in the REF-vector)
LINES = [1 8;       % L01
         9 2;       % L02
         2 3;       % L03
         3 4;       % L04
         4 5;       % L05
         5 6;       % L06
         6 7;       % L07
         17 10;     % L08
         10 12;     % L09
         11 13;     % L10
         12 16;     % L11
         16 15;     % L12
         15 14;     % L13
         19 21;     % L14
         22 18;     % L15
         20 23];    % L16
         
     
    % 0) Normal vectors (length = 1) to the line segments
    [noLines tmp] = size(LINEMODEL);
    for kk = 1:noLines,
		% Rotate pi/2 to get orthogonal vector.
        R = [0 -1;1 0];
		% LINEMODEL [x1 y1 x2 y2]
		% Two points on this line.
		v = [LINEMODEL(kk, 1:2)];
		u = [LINEMODEL(kk, 3:4)];
		V = R*(u-v)';
		V = V';
		U(kk, 1:2) = V / sqrt(dot(V, V));
		% Distance between the origin and the line model.
		RI(kk, 1) = dot(U(kk, 1:2), v);
    end;
    
    % World coordinate [N x 2]; N is the number of image points
    imagePoints = zeros(size(DIS, 2), 2);
	B = zeros(3, 1);
    % Coordinate transform
    for i = 1:size(imagePoints,1)
        % 1.1) Relative measurements => Sensor co-ordinates
        x = DIS(i)*cos(ANG(i));
        y = DIS(i)*sin(ANG(i));
        % 1.2) Sensor co-ordinates => Robot co-ordinates
        R = [cos(alpha) -sin(alpha) x_offset; ...
            sin(alpha) cos(alpha) y_offset; ...
            0 0 1];
        % Sensor coordinate
        % [+ - 1]
        Xs = R*[x y 1]';
        % 1.3) Robot co-ordinates => World co-ordinates
        R = [cos(Theta) -sin(Theta) X; ...
            sin(Theta) cos(Theta) Y; ...
            0 0 1];
        tmp = R*[Xs(1) Xs(2) 1]';
        imagePoints(i, 1:2) = tmp(1:2,:)';
    end
    % Get rid of outliers.
    for i = 1:size(imagePoints,1)
        distanceToEachLine = zeros(size(U,1),1);
        for j = 1:noLines
            distanceToEachLine(j)= abs(RI(j) - ...
                abs(dot(U(j, :), imagePoints(i, :))));
            % We are dealing with line segments.
            % LINEMODEL [x1 y1 x2 y2]
            % Two ends of this line segment.
            u = [LINEMODEL(j, 1:2)];
            v = [LINEMODEL(j, 3:4)];
            imagePoint_u = imagePoints(i,:) - u;
            imagePoint_v = imagePoints(i,:) - v;
            to_u = dot(imagePoint_u, imagePoint_u);
            to_v = dot(imagePoint_v, imagePoint_v);
            if to_u > to_v
                longSide = to_u;
                shortSide = to_v;
            else
                longSide = to_v;
                shortSide = to_u;
            end
            u_v = u - v;
            if longSide - distanceToEachLine(j)^2 > dot(u_v, u_v)
                distanceToEachLine(j) = sqrt(shortSide);
            end
        end
        [distance(i,1) target(i)] = min(distanceToEachLine);
    end
	% Construct continuous new imagePoints.
    j = 1;
    for i = 1:size(distance, 1),
        if distance(i) < DISTANCE_THRESHOLD
            imagePoints_new(j, 1:2) = imagePoints(i,:);
            j = j+1;
        end
    end
    imagePoints = imagePoints_new;
    distance = zeros(size(imagePoints), 1);
    % REPEAT UNTIL THE PROCESS CONVERGE
    for iteration = 1:max_iterations,
		% Get average value along row dimension. [1 x 2]
        imagePoints_mean = mean(imagePoints, 1);
        % Translate and rotate data points
        imagePoints_new = zeros(size(imagePoints));
        for i = 1:size(imagePoints,1)
            imagePoints_new(i, 1:2) = [B(1) B(2)] + ...
                ([cos(B(3)) -sin(B(3)); sin(B(3)) cos(B(3))] ...
                * (imagePoints(i, :) - imagePoints_mean)')' ...
                + imagePoints_mean;
        end
		% What's the result of above manipulation.
        figure(5);clf();
        plot_line_segments(REF, LINES, 1);
        hold on;
        plot(imagePoints_new(:,1), imagePoints_new(:,2), 'bo');
        imagePoints = imagePoints_new;
        imagePoints_mean = mean(imagePoints, 1);
        % 2) -------------- Find targets for data points
        for i = 1:size(imagePoints,1)
            distanceToEachLine = zeros(size(U,1),1);
            for j = 1:noLines
                distanceToEachLine(j)= abs(RI(j) - ...
						abs(dot(U(j, :), imagePoints(i, :))));
            end
			[distance(i, 1) target(i)] = min(distanceToEachLine);
            distance(i, 1) = RI(target(i)) - ...
                    abs(dot(U(target(i), :), imagePoints(i, :)));
        end

        % Set up linear equation system, 
        if sum(distance < DISTANCE_THRESHOLD) == 0
            error('threshold too small')
        end
      
		for i = 1:size(distance, 1)
            X1(i) = U(target(i),1);
            X2(i) = U(target(i),2);
            X3(i) = U(target(i), :) * [0 -1; 1 0] ...
                * (imagePoints(i, :) - imagePoints_mean)';
        end
		A = [X1; X2; X3];
		% [imagePoints.row x 3]
		A = A';
		% Note you have to handle situations then det(A'*A) == 0
		% TODO How? When will this happend?
		B = inv(A'*A)*A'*distance;
		% Calculate the variance of the fit!
		% TODO why substract 4?
		variance = dot(distance - A*B, distance - A*B) ...
			/(size(distance,1)-4);
		C = variance * inv(A'*A);

		% Add latest contribution to the overall congruence
		ddx = ddx + B(1,1);
		ddy = ddy + B(2,1);
		dda = dda + B(3,1);

		% Check if the process has converged
        first_cond = sqrt(B(1,1)^2 + B(2,1)^2) < 5;
        second_cond = abs(B(3,1)) < 0.1*pi/180;
		if ( first_cond && second_cond )
			break;
        end
    end
	% Check if max number off iterations is reached, i.e. no congurance!
	if iteration == max_iterations
		ddx = 0;
		ddy = 0;
		dda = 0;
		C(1) = -1;
    end
